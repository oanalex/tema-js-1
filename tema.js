var date = [
	["A1","B1","C1"],
	["A1","B1","C2"],
	["A1","B1","C3"],
	["A1","B2","C4"],
	["A1","B2","C5"],
	["A1","B3","C6"],
	["A2","B4","C7"],
	["A2","B5","C8"],
	["A2","B5","C9"],
	["A3","B6","C10"]
]

//var optionsA = ["Toate", "A1", "A2", "A3"];


var selectA = document.getElementById("A");
var selectB = document.getElementById("B");
var selectC = document.getElementById("C");

var tabelListare = document.getElementById("list-filter");

var textA = "";
var textB = "";
var textC = "";

var optionsA = "";
var optionsB = "";
var optionsC = "";

var result = [];

function listTabel(data) {
	for (var i = 0; i < date.length; i++) {
		var currentLine = date[i];
		var row = tabelListare.appendChild(document.createElement('div'));
		row.className = "list-row";
		row.innerHTML = currentLine;		
	}
}

function deleteTabel() {
	if (tabelListare.hasChildNodes()) {
		tabelListare.innerHTML = "";
	}
}


function selectElem(optionsToList, elemToList) {

	if (elemToList === "Toate") {

		for (var x = 0; x < optionsToList.length; x++) {
			var currentOption = optionsToList[x];
			
			for (var y = 0; y < date.length; y++) {
				var currentLine = date[y];
				
				for (var z = 0; z < currentLine.length; z++) {
					var currentColumn = currentLine[z];

					if (currentOption.innerHTML === currentColumn) {

					var row = tabelListare.appendChild(document.createElement('div'));
					row.className = "list-row";
					row.innerHTML = currentLine;
					result.push(currentLine[0], currentLine[1], currentLine[2]);											
					}
				}
			}
		}
		//console.log(result);
		return result;
	}else if (elemToList !== "Toate") {
		for (var i = 0; i < date.length; i++) {
			var currentLine = date[i];
			for (var j = 0; j < currentLine.length; j++) {
				var currentColumn = currentLine[j];
				if (elemToList === currentColumn) {
					var row = tabelListare.appendChild(document.createElement('div'));
					row.className = "list-row";
					row.innerHTML = currentLine;
					result.push(currentLine[0], currentLine[1], currentLine[2]);
				} 
			}
		}
		return result;
	} 

}





function selectOption(select, options, result) {
	optionsA = document.querySelectorAll("#A option");
	optionsB = document.querySelectorAll("#B option");
	optionsC = document.querySelectorAll("#C option");	
	for (var i = options.length-1; i > -1; i--) {
		var currentOption = options[i];
		for (var j = 0; j < result.length; j++) {
			var currentResult = result[j];
			if (currentOption.innerHTML === currentResult) {
				select.value = currentOption.innerHTML;
			}			
		}		
	}
}

function deleteOptions(options, result) {
	optionsA = document.querySelectorAll("#A option");
	optionsB = document.querySelectorAll("#B option");
	optionsC = document.querySelectorAll("#C option");	
	var counter = 0;
	for (var i = options.length-1; i > -1; i--) {
		var currentOption = options[i];	
		for (var j = 0; j < result.length; j++) {
			var currentResult = result[j];
			if (currentOption.innerHTML === currentResult) {
				break;
			}
			if (currentResult === result[result.length-1]) {
				counter += 1;	
				if (currentOption !== options[0]) {
					currentOption.parentNode.removeChild(currentOption);					
				} else if (currentOption === options[0] && options.length - counter <= 1) {
					currentOption.parentNode.removeChild(currentOption);					
				}	
			}		
		}
	}
}

function addOptions(select, options, result) {
	optionsA = document.querySelectorAll("#A option");
	optionsB = document.querySelectorAll("#B option");
	optionsC = document.querySelectorAll("#C option");	
	var uniqueResult = Array.from(new Set(result));
	var letterToSearch = options[options.length-1].innerHTML.substr(0, 1);

	for (var i = 0; i < uniqueResult.length; i++) {
		var currentUniqueResult = uniqueResult[i];
		if (currentUniqueResult.indexOf(letterToSearch) === 0 ) {
			for (var j = 0; j < options.length; j++) {
				var currentOption = options[j];
				if (currentUniqueResult === currentOption.innerHTML) {
					break;


				} 
				if (currentOption === options[options.length - 1]) {
					console.log(currentUniqueResult + " = " + currentOption.innerHTML);
					var newOption = select.appendChild(document.createElement("option"));
					newOption.innerHTML = currentUniqueResult;
				}
			}
		}
	}

}



listTabel(date);

selectA.addEventListener("change", function() {
	deleteTabel();
	textA = selectA.options[selectA.selectedIndex].text;
	optionsA = document.querySelectorAll("#A option");
	optionsB = document.querySelectorAll("#B option");
	optionsC = document.querySelectorAll("#C option");
	selectElem(optionsA, textA);	
	if (textA === "Toate") {
		addOptions(selectB, optionsB, result);
		addOptions(selectC, optionsC, result);
	
	}

	deleteOptions(optionsB, result);
	deleteOptions(optionsC, result);


})

selectB.addEventListener("change", function() {
	deleteTabel();
	textB = selectB.options[selectB.selectedIndex].text;
	optionsB = document.querySelectorAll("#B option");
	optionsA = document.querySelectorAll("#A option");
	optionsC = document.querySelectorAll("#C option");		
	selectElem(optionsB, textB);
	if (textB === "Toate") {
		addOptions(selectC, optionsC, result);		
	}


	selectOption(selectA, optionsA, result);
	deleteOptions(optionsC, result);
	
})

selectC.addEventListener("change", function() {
	deleteTabel();
	textC = selectC.options[selectC.selectedIndex].text;
	optionsA = document.querySelectorAll("#A option");
	optionsB = document.querySelectorAll("#B option");
	optionsC = document.querySelectorAll("#C option");		
	selectElem(optionsC, textC);
	selectOption(selectA, optionsA, result);
	selectOption(selectB, optionsB, result);	
})